#/bin/bash

print_ordinal() {
	n=$(date +"%d")
	 
	if [ $n -ge 11 -a $n -le 13 ] ; then
	  echo "th"
	else
	 case $(( $n%10 )) in
	 1)
	   echo st
	   ;;
	 2)
	   echo nd
	   ;;
	 3)
	   echo rd
	   ;;
	 *)
	   echo th
	   ;;
	 esac
	fi
}
hostname=""
email=""
days=""

usage="Usage:\n$(basename "$0") -h hostname -p port -e email@x.y [-d days]
A hostname and email address are required.
You can also optionally specify how many days
before should be checked for expiry, but if
you don't, then it defaults to 21 days.\n"

while getopts ":h:p:e:d:" opt; do
  case $opt in
     h)
       echo "The hostname is $OPTARG" >&2
       hostname=$OPTARG
       ;;
     p)
       echo "The port number on the server is $OPTARG" >&2
       port=$OPTARG
       ;;
     e)
       echo "The email address is $OPTARG" >&2
       email=$OPTARG
       ;;
     d)
       echo "The number of days to check is $OPTARG" >&2
       days=$OPTARG
       ;;
     *)
       echo "invalid command: no parameter included with argument $OPTARG"
       printf "$usage"
       ;;
  esac
done

shift "$(( OPTIND - 1 ))"

if [ -z "$hostname" ] || [ -z "$port" ] || [ -z "$email" ]; then
        echo 'Missing the hostname, port or email address' >&2
        printf "$usage"
        exit 1
fi


DOM=$hostname
PORT=$port
# 7 days in seconds 
ONEDAY="86400"
DAYS=${days:-21}

ord=`print_ordinal`
date=`/bin/date "+%A, %d$ord of %B %Y - %H:%M:%S"`

echo
echo $DOM certificate check on the $date

SECONDS=`echo 86400*$DAYS | bc`
echo seconds = $SECONDS
echo | openssl s_client -connect $DOM:$PORT \
| openssl x509 -noout -checkend "$SECONDS"


if [ $? -eq 1 ]; then
	echo The certificate for $DOM will expire in less than $DAYS days
	echo The certificate for $DOM dovecot service is expiring soon, so will \
	restart it | mailx -s "Cert check from the MPZ server" $email
	/sbin/service dovecot restart
else
	echo The certificate for $DOM has more than $DAYS days left on it
fi
