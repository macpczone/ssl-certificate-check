# ssl-certificate-check

This script can be used to check if your email server certificate has expired and also restart the service in case a new certificate has been downloaded.